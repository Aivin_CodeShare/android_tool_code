package com.walkera.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.walkera.bean.PrecheckItem;
import com.walkera.wktools.tools.WkLogTool;
import com.zc.walkera.casic.R;
import java.util.List;
import java.util.Locale;


/**
 *  RecyclerView  +  RadioGroup RadioButton  ,防止数据错乱 处理
 */
public class PreFlightCheckAdapter extends RecyclerView.Adapter {
    private List<PrecheckItem> itemsValus;

    public PreFlightCheckAdapter(  List<PrecheckItem> itemsValus) {
        this.itemsValus = itemsValus;
    }

    @Override
    public ChooseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_preflight_check, parent, false);
        return new ChooseHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,final int position) {
        final ChooseHolder chooseHolder = (ChooseHolder)holder;
        PrecheckItem bean =  itemsValus.get(position);
        String  value =  String.format(Locale.getDefault() ,"%d、%s" ,position+1 ,bean.itemInfo);
        chooseHolder.tvCheckItem.setText(value);

        // 解决显示混乱的问题
        chooseHolder.precheckRadioGroup.setOnCheckedChangeListener(null);
        PrecheckItem itemBean = itemsValus.get(position);
        if(itemBean.checkIndex ==1){
            chooseHolder.precheckRadioGroup.check(R.id.precheckOk);
        }else if(itemBean.checkIndex==2){
            chooseHolder.precheckRadioGroup.check(R.id.precheckNo);
        }else{
            chooseHolder.precheckRadioGroup.clearCheck( );
        }

        chooseHolder.precheckRadioGroup.setId(position); // 设置id
        chooseHolder.precheckRadioGroup.setOnCheckedChangeListener( new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                PrecheckItem bean = itemsValus.get( group.getId()); // 通过 id 做桥梁，来获得对应的数据
                switch (checkedId) {
                    case R.id.precheckOk:
                        bean.checkIndex = 1;
                        break;
                    case R.id.precheckNo:
                        bean.checkIndex = 2;
                        break;

                }
            }
        });
    }



    @Override
    public int getItemCount()  {
        return itemsValus.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class ChooseHolder extends RecyclerView.ViewHolder {

        private TextView tvCheckItem;
        private RadioGroup precheckRadioGroup ;
         private ChooseHolder(View itemView){
            super(itemView);
            tvCheckItem =   itemView.findViewById(R.id.tvCheckItem);
            precheckRadioGroup =   itemView.findViewById(R.id.precheckRadioGroup);
        }
    }

}
