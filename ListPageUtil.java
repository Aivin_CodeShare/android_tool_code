import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *  list 分页工具
 */
public final class ListPageUtil {

    /**
     * 写入和写入之间会进行同步等待的 List。
     */
    private final CopyOnWriteArrayList<String> list;
    /**
     * 最终的每次加载 pageSize 条
     */
    private final int pageSize;

    /**
     * 总共有多少页数据
     */
    private final int pageCount ;
    /**
     * 原始所有数据合集 的大小
     */
    private final int listSize ;
    /**
     * 当前页数 , 从 0 开始
     */
    private int pageIndex;


    /**
     *
     * @param list 所有数据集合
     * @param pageSize  每次加载 pageSize 条
     */
    public ListPageUtil(List<String> list, int pageSize) {
        if (null == list || 0 == list.size())  {
            throw new UnsupportedOperationException("List can not null!");
        }

        if (pageSize <=0 )   {
            throw new UnsupportedOperationException("Page size must be > 0");
        }

        this.list = new CopyOnWriteArrayList<>(list);
        this.listSize = list.size();
        this.pageSize = Math.min(pageSize, listSize);

        int tempPageCount =  listSize / pageSize;
        // 如果除不尽，不满一页的数据 也要算一页
        this.pageCount = listSize % pageSize == 0 ? tempPageCount : tempPageCount + 1;
        this.pageIndex = 0;
    }


    /**
     * 获取下一页 <br>
     * 通过 list.subList 获得分页数据
     **/
    public List<String> nextPage()  {
        boolean hasNext = hasNext();
        if(!hasNext){
            return null ;
        }

        List<String> subList ;
        if (pageIndex == pageCount - 1) {
            subList = new ArrayList<>(list.subList(pageIndex * pageSize, listSize));
        }else  {
            subList = new ArrayList<>( list.subList(pageIndex * pageSize, (pageIndex + 1) * pageSize));
        }
        pageIndex++;
        return subList;
    }


    /**
     * 自己写逻辑，获取分页数据
     */
    public List<String> nextPageWithMlz()  {
        List<String> subList;
        int remain;
        int offset = pageIndex * pageSize;
        if (pageIndex == pageCount - 1) {
            remain = listSize - offset;
            //subList = list.subList(index * pageSize, listSize);
        } else  {
            remain = pageSize;
            //subList = list.subList(index * pageSize, (index + 1) * pageSize);
        }
        subList = new ArrayList<>();
        for (int i=0; i<remain; ++i) {
            String str = list.get(i + offset);
            subList.add(str);
        }

        pageIndex++;
        return subList;
    }

    public List<String> getPageData(int pageIndex)  {
        boolean hasNext = hasNext(pageIndex);
        if(!hasNext){
            return null ;
        }
        List<String> subList ;
        if (pageIndex == pageCount - 1) {
            subList = new ArrayList<>(list.subList(pageIndex * pageSize, listSize));
        }else  {
            subList = new ArrayList<>( list.subList(pageIndex * pageSize, (pageIndex + 1) * pageSize));
        }
        return subList;
    }


    /**
     * 是否还有下一页
     */
    public boolean hasNext() {
        return pageIndex < pageCount;
    }

    public boolean hasNext(int currentPageIndex) {
        return currentPageIndex < pageCount;
    }

}
