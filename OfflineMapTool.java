package com.bgy.offlinecontroler.tool;

import com.bgy.utils.logutil.WkLog;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.TileSystem;

/**
 * 坐标转换工具
 * @author Aivin
 * created on 2021/11/1
 * des:
 */
public class OfflineMapTool {

    private static final double pi = 3.1415926535897932384626;
    // 扁率
    private static final double  ee = 0.00669342162296594323;
    // 长半轴
    private static final double a = 6378245.0;

    /**
     * GCJ02(火星坐标系) 转 GPS84 (地球坐标)
     * @param lng , longitude  火星坐标系  经度
     * @param lat  , latitude 火星坐标系 纬度
     * @return GeoPoint geoPoint1 = new GeoPoint( result[1] ,result[0]);
     */
    public static double[] gcj02towgs84(double lng, double lat) {
        double dlat = transformlat(lng - 105.0, lat - 35.0);
        double dlng = transformlng(lng - 105.0, lat - 35.0);
        double radlat = lat / 180.0 * pi;
        double magic = Math.sin(radlat);
        magic = 1 - ee * magic * magic;
        double sqrtmagic = Math.sqrt(magic);
        dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi);
        dlng = (dlng * 180.0) / (a / sqrtmagic * Math.cos(radlat) * pi);
        double mglat = lat + dlat;
        double mglng = lng + dlng;
        return new double[]{lng * 2 - mglng, lat * 2 - mglat};
    }

    private static double transformlat(double lng, double lat) {
        double ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));
        ret += (20.0 * Math.sin(6.0 * lng * pi) + 20.0 * Math.sin(2.0 * lng * pi)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(lat * pi) + 40.0 * Math.sin(lat / 3.0 * pi)) * 2.0 / 3.0;
        ret += (160.0 * Math.sin(lat / 12.0 * pi) + 320 * Math.sin(lat * pi / 30.0)) * 2.0 / 3.0;
        return ret;
    }

    private static double transformlng(double lng, double lat) {
        double ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));
        ret += (20.0 * Math.sin(6.0 * lng * pi) + 20.0 * Math.sin(2.0 * lng * pi)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(lng * pi) + 40.0 * Math.sin(lng / 3.0 * pi)) * 2.0 / 3.0;
        ret += (150.0 * Math.sin(lng / 12.0 * pi) + 300.0 * Math.sin(lng / 30.0 * pi)) * 2.0 / 3.0;
        return ret;
    }


    /**
     * 判断 GeoPoint 经纬度 范围是否合法
     */
    public static boolean checkGeoPointIsValid(GeoPoint point){
        return isValidGeoPoint(point.getLatitude(),point.getLongitude(),point.getLatitude(),point.getLongitude()) ;
    }

    /**
     * 判断 aLatitude, aLongitude 范围是否合法
     */
    public static boolean checkGeoPointIsValid(double aLatitude, double aLongitude){
        return isValidGeoPoint(aLatitude,aLongitude,aLatitude,aLongitude) ;
    }

    private static boolean isValidGeoPoint(final double north, final double east, final double south, final double west) {
        final TileSystem tileSystem = org.osmdroid.views.MapView.getTileSystem();
        if (!tileSystem.isValidLatitude(north)){
            WkLog.showErrorLog("north must be in " + tileSystem.toStringLatitudeSpan());
            return false ;
        }
        if (!tileSystem.isValidLatitude(south)){
            WkLog.showErrorLog("south must be in " + tileSystem.toStringLatitudeSpan());
            return false ;
        }
        if (!tileSystem.isValidLongitude(west)){
            WkLog.showErrorLog("west must be in " + tileSystem.toStringLongitudeSpan());
            return false ;
        }

        if (!tileSystem.isValidLongitude(east)){
            WkLog.showErrorLog("east must be in " + tileSystem.toStringLongitudeSpan());
            return false ;
        }
        return true ;
    }


}
