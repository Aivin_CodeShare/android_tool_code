package com.example.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class WkProgressView extends View {
	private Paint mPaint;
	private int mWidth,mHeight;
	private RectF rectBlackBg =null ;
	private double currentValue =3;
	private final PorterDuffXfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.DST_OUT) ;
	private final Rect rectForLine = new Rect( ) ;
	private final int sumCount =20; // 总item
	private ShapeDrawable drawableFull  ;
	private ShapeDrawable drawableNotFull  ;

	public WkProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initView();
	}

	public WkProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public WkProgressView(Context context) {
		super(context);
		initView();
	}


	private void initView( ){
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

		if (widthSpecMode == MeasureSpec.EXACTLY || widthSpecMode == MeasureSpec.AT_MOST) {
			mWidth = widthSpecSize;
		} else {
			mWidth = 0;
		}

		if (heightSpecMode == MeasureSpec.AT_MOST || heightSpecMode == MeasureSpec.UNSPECIFIED) {
			mHeight = 30 ; //dipToPx(15);
		} else {
			mHeight = heightSpecSize;
		}

		setMeasuredDimension(mWidth, mHeight);
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int mRadius = mHeight/2;

		// 灰色背景
		mPaint.setColor(Color.parseColor("#F2F2F2"));
		if(rectBlackBg==null){
			rectBlackBg = new RectF( );
		}
		rectBlackBg.left = 0;
		rectBlackBg.top =0;
		rectBlackBg.right =mWidth ;
		rectBlackBg.bottom =mHeight ;
		canvas.drawRoundRect(rectBlackBg, mRadius, mRadius, mPaint);

		// 蓝色进度
		double progressFloat =currentValue/sumCount ;
		ShapeDrawable drawable ;
		if(progressFloat==1){
			drawable = getDrawableFull(mRadius) ;
		}else{
			drawable = getDrawableNotFull(mRadius) ;
		}

		drawable.getPaint().setColor( Color.parseColor("#2fb855"));
		drawable.setBounds(0, 0, (int) (mWidth* progressFloat), mHeight);
		drawable.draw(canvas);

		// 白色间隔
		int lineWidht= 10 ;
		float oneWidth= (mWidth *1f - (sumCount-1)*lineWidht)  / sumCount  ;
		for(int i=1 ;i< currentValue ;i++){
			rectForLine.left= (int) ( (oneWidth   * i) + (i-1) *lineWidht);
			rectForLine.top=0;
			rectForLine.right =rectForLine.left+lineWidht ;
			rectForLine.bottom=mHeight;
			mPaint.setColor(Color.RED);
			canvas.drawRect(rectForLine ,mPaint);
		}

		// 圆角处截取多余的红线
		int layerId = canvas.saveLayer(0, 0, this.getWidth(), this.getHeight(), mPaint, Canvas.ALL_SAVE_FLAG);//保存图层
		mPaint.setColor(Color.WHITE); // 假像
		canvas.drawRect(0,0 ,getWidth() ,getHeight() ,mPaint);
		mPaint.setXfermode(xfermode);//PorterDuffXfermode 设置画笔的图形混合模式
		canvas.drawRoundRect(rectBlackBg, mRadius, mRadius, mPaint);
		mPaint.setXfermode(null);
		canvas.restoreToCount(layerId);
	}



	private ShapeDrawable getDrawableFull(int mRadius){
		if(drawableFull==null){
			drawableFull = new ShapeDrawable(  getRoundRectShap(mRadius, mRadius, mRadius, mRadius) );
		}
		return drawableFull ;
	}

	private ShapeDrawable getDrawableNotFull(int mRadius){
		if(drawableNotFull==null){
			drawableNotFull =new ShapeDrawable( getRoundRectShap(mRadius, 0, 0, mRadius) );
		}
		return drawableNotFull ;
	}

	private   RoundRectShape getRoundRectShap(int leftTop, int rightTop, int rightBottom, int leftBottom){
		float []  outerRadii = new float[8];
		if (leftTop > 0) {
			outerRadii[0] = leftTop;
			outerRadii[1] = leftTop;
		}
		if (rightTop > 0) {
			outerRadii[2] = rightTop;
			outerRadii[3] = rightTop;
		}
		if (rightBottom > 0) {
			outerRadii[4] = rightBottom;
			outerRadii[5] = rightBottom;
		}
		if (leftBottom > 0) {
			outerRadii[6] = leftBottom;
			outerRadii[7] = leftBottom;
		}
		return new RoundRectShape(outerRadii, null, null);
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action =event.getAction() ;
		switch (action){
			case  MotionEvent.ACTION_DOWN:
			case  MotionEvent.ACTION_MOVE:
				float width = this.getMeasuredWidth() ;
				float currentX = event.getX() ;
				float progress =  currentX / width;
				currentValue = sumCount * progress   ; //当前值
				currentValue = Math.ceil(currentValue) ; // 向上取整
				currentValue= currentValue>= sumCount ?  sumCount: currentValue;
				currentValue= currentValue <= 0 ?  0: currentValue;
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				performClick();
				break;
		}
		return true;
	}

	@Override
	public boolean performClick() {
		return super.performClick();
	}

}
