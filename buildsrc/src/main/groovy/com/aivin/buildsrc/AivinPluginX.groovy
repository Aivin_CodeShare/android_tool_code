package com.aivin.buildsrc

import com.aivin.buildsrc.myactions.Md5CheckAction
import com.aivin.buildsrc.mytask.FileMd5CheckkTask
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project;

class AivinPluginX implements  Plugin<Project>{

    @Override
    void apply(Project project) {
        FileMd5CheckkTask task = project.getTasks().create(FileMd5CheckkTask.TASK_NAME , FileMd5CheckkTask.class)
        Action action = new Md5CheckAction(project ,task ) ;
        task.doLast(action)
    }

}
