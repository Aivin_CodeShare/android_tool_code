package com.aivin.buildsrc.mytask

import com.aivin.buildsrc.tool.WkLog
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.tasks.Input

//  gradlew fileMd5CheckTask
public class FileMd5CheckkTask extends DefaultTask {
    public static final String TASK_NAME ="fileMd5CheckTask" ;
    private boolean fileMd5Check;

    @Input
    public void isOpenFileMd5Check(boolean fileMd5Check) {
        // 通过 注解 Input +isOpenFileMd5Check ，可以在 gradle 中进行动态配置
        this.fileMd5Check = fileMd5Check;
    }

    boolean getFileMd5Check() {
        return fileMd5Check
    }
}