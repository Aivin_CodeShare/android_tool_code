package wk.demo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;


/**
 * 功能描述：自定义屏幕上下左右手势滑动 。  <br/>
 * 所有的手势 都用view 转交给 探测器 GestureDetector ，ScaleGestureDetector   <br/>
 * 它们都有完整的回调，不要自己去通过计算。   <br/>
 void scrollToLeft( );//向左   <br/>
 void scrollToRight( );//向右   <br/>
 void scrollToTop( );//向上   <br/>
 void scrollToBottom( );//向下   <br/>
 void zoomToMax( float scale);//手势放大   <br/>
 void zoomToMin( float scale);//手势缩小   <br/>
 void zoomStop( ); //停止缩小   <br/>
 void doubleClick( ); 双击   <br/>
 void onClick();  单击   <br/>
 void onLongPress();  //长按
 */
public class GestureView extends View {
    /**最小滑动距离*/
    private final int minVelocity = 5;
    /**
     * 对外的接口
     */
    public MyGestureCallBack myGestureCallBack;
    /**
     * 手势监听
     */
    private GestureDetector gestureDetector;
    /**
     * 缩放监听
     */
    private ScaleGestureDetector scaleGestureDetector = null;

    public GestureView(Context context) {
        super(context);
        initView(context);
    }

    public GestureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }



    private void initView(Context context){
        gestureDetector = new GestureDetector(context, onGestureListener);
        gestureDetector.setOnDoubleTapListener(onDoubleTapListener);
        scaleGestureDetector = new ScaleGestureDetector(context, onScaleGestureListener);
    }

    public void setCallBack(MyGestureCallBack myGestureCallBack) {
        this.myGestureCallBack = myGestureCallBack;
    }


    private ScaleGestureDetector.OnScaleGestureListener onScaleGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scale=detector.getScaleFactor() ;
            if(scale>1){
                myGestureCallBack.zoomToMax(scale);
            }else{
                myGestureCallBack.zoomToMin(scale);
            }
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            // 一定要返回true才会进入onScale()这个函数
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            myGestureCallBack.zoomStop();
        }
    };

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 手势事件交给 Detector接管
        gestureDetector.onTouchEvent(event);
        scaleGestureDetector.onTouchEvent(event);
        performClick();
        return true;
    }


    private GestureDetector.OnDoubleTapListener onDoubleTapListener = new GestureDetector.OnDoubleTapListener() {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            myGestureCallBack.onClick();
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            myGestureCallBack.doubleClick();
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            // 一般可不用
            return false;
        }
    };


    private GestureDetector.OnGestureListener onGestureListener = new GestureDetector.OnGestureListener(){

        @Override
        public boolean onDown(MotionEvent e) {
            // 一般可不用
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            // 一般可不用
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // 一般不用
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            if (distanceX < -minVelocity) {
                myGestureCallBack.scrollToRight( );

            } else if (distanceX > minVelocity) {
                myGestureCallBack.scrollToLeft( );
            } else if (distanceY < -minVelocity) {
                myGestureCallBack.scrollToBottom( );
            } else if (distanceY > minVelocity) {
                myGestureCallBack.scrollToTop( );
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            myGestureCallBack.onLongPress();
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX()  - e2.getX() > minVelocity && Math.abs(velocityX) > minVelocity) {
                myGestureCallBack.scrollToLeft();

            } else if ((e2.getX() - e1.getX() > minVelocity && Math.abs(velocityX) > minVelocity)) {
                myGestureCallBack.scrollToRight();

            } else if (e1.getY()  - e2.getY() > minVelocity && Math.abs(velocityY) > minVelocity) {
                myGestureCallBack.scrollToTop();

            } else if ((e2.getY() - e1.getY() > minVelocity && Math.abs(velocityY) > minVelocity)) {
                myGestureCallBack.scrollToBottom( );
            }
            return true;
        }
    };

    public interface MyGestureCallBack {
        /**
         * 向左
         */
        void scrollToLeft( );

        /**
         *  向右
         */
        void scrollToRight( );

        /**
         *  向上
         */
        void scrollToTop( );

        /**
         * 向下
         */
        void scrollToBottom( );

        /**
         * 手势放大
         */
        void zoomToMax( float scale);

        /**
         * 手势缩小
         */
        void zoomToMin( float scale);

        /**
         * 停止缩小
         */
        void zoomStop( );

        /**
         * 双击
         */
        void doubleClick( );

        /**
         * 单击
         */
        void onClick();

        /**
         * 长按
         */
        void onLongPress();
    }

}





