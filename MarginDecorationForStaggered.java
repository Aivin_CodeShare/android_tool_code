package com.aivin.waterfallflow.bean;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 * 瀑布流的间隔 ItemDecoration
 */
public class MarginDecorationForStaggered extends RecyclerView.ItemDecoration {
    private int margin;
    private int columnCount;
    private int mDividerWidth  ; // 线条的宽度
    private Paint mPaint;


    public MarginDecorationForStaggered(Context context, int columnCount, int margin) {
        this.margin = margin;
        mDividerWidth = margin ;
        this.columnCount = columnCount;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.parseColor("#e76f00"));
    }

    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDraw(canvas, parent, state);
        draw(canvas ,parent);
    }

    private void draw(Canvas canvas, RecyclerView parent) {
        int childSize = parent.getChildCount();
        for (int i = 0; i < childSize; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child.getLayoutParams();

             // 画水平分隔线
            int left = child.getLeft();
            int right = child.getRight();
            int top = child.getBottom() + layoutParams.bottomMargin;
            int bottom = top + mDividerWidth;
            canvas.drawRect(left, top, right, bottom, mPaint);

            // 画垂直分割线
            top = child.getTop();
            bottom = child.getBottom() + mDividerWidth;
            left = child.getRight() + layoutParams.rightMargin;
            right = left + mDividerWidth;
            canvas.drawRect(left, top, right, bottom, mPaint);

            int spanCount = columnCount;
           // 如果是第一行
            if (isfirstRow(i, spanCount)) {
                canvas.drawRect(0, 0, right, mDividerWidth, mPaint);
            }

            //如果是第一列
            if (isfirsColumn(i, spanCount)) {
                canvas.drawRect(0, 0, mDividerWidth, bottom, mPaint);
            }
        }
    }

    /**
     * 判断是不是第一行
     */
    private boolean isfirstRow(int pos, int spanCount) {
        return (pos / spanCount + 1) == 1;
    }


    /**
     * 判断是不是第一列
     */
    private boolean isfirsColumn(int pos, int spanCount) {
        return pos % spanCount == 0;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int left;
        int right = margin;
        int top;
        int bottom = 0;

        StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        int position = params.getSpanIndex();
        if (position == RecyclerView.NO_POSITION) {
            return;
        }

        // top
        if (position < columnCount) {
            top = margin;
        } else {
            top = 0;
        }

        // left
        if (position % columnCount == 0) {
            left = margin;
        } else {
            left = 0;
        }
        outRect.set(left, top, right, bottom);
    }
}
