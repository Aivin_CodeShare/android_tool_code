package com.wk.myapplication;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MyTool {

    public static void getFruitInfo( Class<?> clz ){
        boolean isAnnota = clz.isAnnotationPresent(TestAnnotation.class);
        if(isAnnota){
            TestAnnotation testAnnotation = clz.getAnnotation(TestAnnotation.class);
            System.out.println(clz.getSimpleName()+"--->"+testAnnotation.name());
            System.out.println(clz.getSimpleName()+"--->"+testAnnotation.age());
        }else{
            System.out.println("类没有使用注解" );
        }

        Field[] fields = clz.getDeclaredFields();
        for(Field field :fields){
            field.setAccessible(true);
            // Field field =clz.getDeclaredField("mystudent"); // 获取指定变量的属性
            TestAnnotation testAnnotation1 = field.getAnnotation(TestAnnotation.class);
            if ( testAnnotation1 != null ) {
                System.out.println(clz.getSimpleName()+" ->"+ field.getName()+ "--->"+testAnnotation1.name());
                System.out.println(clz.getSimpleName()+" ->"+ field.getName()+"--->"+testAnnotation1.age());
            }
        }

        Method[] methods = clz.getDeclaredMethods();
        for(Method method : methods){
            method.setAccessible(true);
            //clz.getDeclaredMethod("setInfo" ,int.class ,String.class); // 获取指定方法
            TestAnnotation testAnnotation2 = method.getAnnotation(TestAnnotation.class);
            if ( testAnnotation2 != null ) {
                System.out.println(clz.getSimpleName()+" ->"+ method.getName()+ "--->"+testAnnotation2.name());
                System.out.println(clz.getSimpleName()+" ->"+ method.getName()+"--->"+testAnnotation2.age());
            }
        }
    }
}
