package com.wk.myapplication;

@TestAnnotation (name = "类设置的名字")
public class Person {
    @TestAnnotation( name = "属性设置的名字",age = 22)
    public Student mystudent;

    @TestAnnotation( name = "方法设置的名字",age = 23)
    public void setInfo(int age ,String name){

    }
}
