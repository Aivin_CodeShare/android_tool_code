package com.walkera.map.route.tool;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.walkera.map.route.bean.WkException;
import com.walkera.wktools.tools.WkLogTool;
import java.io.File;

/**
 *  用 高德sdk 加载  本地 mbtiles 文件。
 @Override
 public final Tile getTile(int x, int y, int zoom) {
 byte[] image =null  ;
 try {
 image = yslDataTool.getBytesData( x , y  ,  zoom);
 } catch (WkException e) {
 e.printStackTrace();
 WkLogTool.showLog("以色列 地图异常,   =" +e.getMessage());
 }
 return new Tile(TILE_WIDTH, TILE_HEIGHT, image);
 }
 */
public class MbTilesDataTool {
    private SQLiteDatabase database ;
    private String sql ="" ;

    public MbTilesDataTool(){
        String dbPath = getWkReaderRoot() + "/ysl.mbtiles" ;
        File dbFile = new File(dbPath);
        if(!dbFile.exists()){
            WkLogTool.showLog("请先拷贝数据库库到指定目录 "+dbPath);
        }else{
            database = SQLiteDatabase.openOrCreateDatabase(  dbPath, null);
            sql = "select  *  from tiles  where zoom_level = ?  and tile_column= ? and tile_row = ? ";
        }
    }


    /**
     * @param x 列
     * @param  y  行
     * @param zoom 级别
     * @return byte[] V
     * @throws WkException WkException
     */
    public byte[] getBytesData( int x ,  int  y , int  zoom ) throws WkException {
       if(database==null){
           throw new WkException(1001, "database is null ,please check the db path");
       }

       y = displaceY(zoom ,y); //  this code is important
       String tile_column = String.valueOf(x);
       String tile_row =  String.valueOf(y);
       String zoom_level =  String.valueOf(zoom);

        Cursor cursor=database.rawQuery(sql , new String[]{ zoom_level , tile_column , tile_row});
        byte[] jpgData = null ;
        while(cursor.moveToNext()){
            jpgData=cursor.getBlob(cursor.getColumnIndex("tile_data"));
        }
        cursor.close();
        return jpgData ;
    }

    /**
     * 把 高德的行号 y  转成符合 MBTiles 规范的行号
     * @param zoom 级别
     * @param y 行号
     */
    private  int displaceY(int zoom, int y) {
        if (zoom < 0) {
            return y;
        }
        return (1 << zoom) - y - 1;
    }


    public void close(){
        if(database!=null){
            database.close();
        }
    }

    private String getWkReaderRoot() {
        String lastPath  ;
        boolean hasSd = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ;
        File root =hasSd ?  Environment.getExternalStorageDirectory():  Environment.getDataDirectory() ;
        lastPath = root +  "/walkera/ysl_db";

        File file = new File(lastPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return lastPath;
    }

}
